package a01.MathFunc;


public class MathFunc_Sqrt {
	
    private static final double ZERO = 0.0;
    private static final double FIVE = 5.0;

    


	public interface DoubleMethod {
		public double compute(double value);
	}

	public static void main(String[] args) {
		MathFunc_Sqrt obj = new MathFunc_Sqrt();
		obj.start_classic();
		obj.start_lambda();
	}

	/* Mathematische Funktion mit anonymer Klasse */
	private void start_classic() {

		// anonyme Klasse
		DoubleMethod meth = new DoubleMethod() {
			public double compute(double v) {
				return Math.sqrt(v);
			}
		};

		System.out.println("Wurzelberechnung mit klassischer anonymer Klasse");
		print(meth);
	}

	/* Mathematische Funktion mit Lambda-Ausdruck */

	private void start_lambda() {
		DoubleMethod meth = (double v) -> {
			return Math.sqrt(v);
		};
		System.out.println("\n Wurzelberechnung mit Lambda-Ausdruck");
		print(meth);
	}
	
	private void print(DoubleMethod meth){
		for (double x = ZERO; x <= FIVE; x += 1) {
			System.out.println(x + "->" + meth.compute(x));
		}
	}

}
