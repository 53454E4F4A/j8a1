package a01.MathFunc;

import java.util.Arrays;
import java.util.List;

public class MathFunc {

	private double bBorder;
	private double tBorder;
	private int steps;
	private List<Double> data = Arrays.asList(new Double[] { 0., 1., 2., 3.,
			4., 5., 6., 6. });
	private int i = 0;

	private double h = 0.0;

	public static void main(String args[]) {
		MathFunc f = new MathFunc();
		f.setbBorder(0);
		f.settBorder(8);
		f.setSteps(10);
		f.setH();

		f.generateValuesOfX();
	}

	public double getbBorder() {
		return bBorder;
	}

	public void setH() {
		h = (bBorder - tBorder) / steps;
	}

	public void setbBorder(double bBorder) {
		this.bBorder = bBorder;
	}

	public double gettBorder() {
		return tBorder;
	}

	public void settBorder(double tBorder) {
		this.tBorder = tBorder;
	}

	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}

	public double getH() {
		return h;
	}

	private void generateValuesOfX() {
		//
		data.forEach((element) -> (element = this.tBorder + (i++) * h));
		data.forEach(element -> (System.out.println(element)));
	}

}
