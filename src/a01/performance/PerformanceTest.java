package a01.performance;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 */

public class PerformanceTest {
	private static final int ANFANG = 1000;
	private static final double ENDE = Math.pow(10, 6);
	private static final int FAKTOR = 10;
	private static final double SEC = Math.pow(10, 9);
	private static final int VALUE = 50;
	private static final int SLEEP = 1000;

	public static void main(String[] args) throws InterruptedException {

		CollectionsExample collExamp = new CollectionsExample();
		performanceRemove(collExamp);
		System.out.println("-----------------------------------\n");
		performanceSort(collExamp);

	}

	/**
	 * 
	 * Tests the performance by removing objects from the given Arraylist.
	 * 
	 * @param collExamp
	 *            The object that contains the Arraylist.
	 * @throws InterruptedException
	 */
	private static void performanceRemove(CollectionsExample collExamp) throws InterruptedException {
		for (int i = ANFANG; i <= ENDE; i = i * FAKTOR) {

			collExamp.fillAray(i);

			List<Integer> tmpList = new ArrayList<>(collExamp.getList());
			System.out.println("ArrayList size is: " + i);

			Thread.sleep(SLEEP);

			long before = System.nanoTime();
			collExamp.removeWithoutLambda(VALUE);
			long after = System.nanoTime();

			System.out.printf("Remove without Lambda: %f s\n", (after - before) / SEC);

			collExamp.setList(tmpList);

			Thread.sleep(SLEEP);

			before = System.nanoTime();
			collExamp.removeWithLambda(VALUE);
			after = System.nanoTime();

			System.out.printf("Remove with Lambda: %11f s\n\n", (after - before) / SEC);

		}
	}

	/**
	 * 
	 * Tests the performance by sorting the objects in ascending order.
	 * 
	 * @param collExamp
	 *            The object that contains the Arraylist.
	 * @throws InterruptedException
	 */
	private static void performanceSort(CollectionsExample collExamp) throws InterruptedException {
		for (int i = ANFANG; i <= ENDE; i = i * FAKTOR) {

			collExamp.fillAray(i);
			List<Integer> tmpList = new ArrayList<>(collExamp.getList());

			System.out.println("ArrayList size is: " + i);

			Thread.sleep(SLEEP);

			long before = System.nanoTime();
			collExamp.notParallelizedClassic();
			long after = System.nanoTime();
			System.out.printf("Sort not parallelized: %18f s\n", (after - before) / SEC);

			collExamp.setList(tmpList);

			Thread.sleep(SLEEP);

			before = System.nanoTime();
			collExamp.parallelizedStream();
			after = System.nanoTime();
			System.out.printf("Sort parallelized stream: %15f s\n", (after - before) / SEC);

			Thread.sleep(SLEEP);

			collExamp.setList(tmpList);

			before = System.nanoTime();
			collExamp.parallelizedLambda();
			after = System.nanoTime();
			System.out.printf("Sort parallelized stream Lambda: %f s\n", (after - before) / SEC);

			Thread.sleep(SLEEP);

			collExamp.setList(tmpList);

			before = System.nanoTime();
			collExamp.serielStreamLambda();
			after = System.nanoTime();
			System.out.printf("Sort stream with Lambda: %16f s\n", (after - before) / SEC);

			Thread.sleep(SLEEP);

			collExamp.setList(tmpList);

			before = System.nanoTime();
			collExamp.serielStreamAnonymusClass();
			after = System.nanoTime();
			System.out.printf("Sort stream with anonymus class: %f s \n\n", (after - before) / SEC);
		}
	}

}
