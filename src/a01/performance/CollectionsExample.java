package a01.performance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 *
 */
public class CollectionsExample {

	private List<Integer> list = new ArrayList<>();

	/**
	 * Generates a random number in a certain range of values.
	 * 
	 * @param min
	 *            lower limit
	 * @param max
	 *            upper limit
	 * @return the random number
	 */
	private static int randInt(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}

	/**
	 * Fills the array with random numbers.
	 * 
	 * @param size
	 *            number of elements to be added in the list
	 */
	public void fillAray(int size) {
		for (int i = 0; i < size; i++) {
			list.add(randInt(0, 100));
		}
	}

	/**
	 * Removes elements from the list which are greater then {@code value}.
	 * These method uses the classic way of implementing a remove function.
	 * 
	 * @param value
	 *            sets the upper limit
	 */
	public void removeWithoutLambda(int value) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i) > value) {
				list.remove(i--);
			}
		}
	}

	/**
	 * Removes elements from the list which are greater then {@code value}.
	 * Implements the removeIf method using a lambda expression.
	 * 
	 * @param value
	 *            sets the upper limit
	 */
	public void removeWithLambda(int value) {
		list.removeIf(x -> x > value);
	}

	/**
	 * 
	 * @return the Arraylist
	 */
	public List<Integer> getList() {
		return list;
	}

	/**
	 * Assigns a new list to the object.
	 * 
	 * @param list
	 *            the list which will be assigned
	 */
	public void setList(List<Integer> list) {
		this.list = list;
	}

	/**
	 * Sorts the list using a parallelized stream with a lambda expression.
	 */
	public void parallelizedLambda() {
		list.parallelStream().sorted((o1, o2) -> o1 - o2);
	}

	/**
	 * Sorts the list using an anonymous class.
	 */
	public void notParallelizedClassic() {
		list.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1 - o2;
			}
		});
	}

	/**
	 * Sorts the list using a parallelized stream with an anonymous inner class.
	 */
	public void parallelizedStream() {
		list.parallelStream().sorted(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1 - o2;
			}
		});
	}

	/**
	 * Sorts the list using a stream with a lambda expression.
	 */
	public void serielStreamLambda() {
		list.stream().sorted((o1, o2) -> o1 - o2);
	}

	/**
	 * Sorts the list using a stream with an anonymous class.
	 */
	public void serielStreamAnonymusClass() {
		list.stream().sorted(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1 - o2;
			}
		});
	}
}
