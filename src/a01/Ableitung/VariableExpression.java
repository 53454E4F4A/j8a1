package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Variable.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class VariableExpression extends BaseExpression {

	VariableExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		// TODO Auto-generated constructor stub
		this.symbol = "x";
		this.diff = () -> new ValueExpression(1);
		this.eval = () -> null;
		this.dupe = () -> new  VariableExpression(null, null);
	}

	@Override
	String toInfix() {
		// TODO Auto-generated method stub
		return this.symbol;
	}



}
