package a01.Ableitung;
/**
 * Diese Klasse modeliert ein Literal.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class ValueExpression extends BaseExpression {
	double value;
	
	ValueExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		// TODO Auto-generated constructor stub
		this.diff = () -> new ValueExpression(0);
		this.eval = () -> value;
		this.dupe = () -> this;
	}

	public ValueExpression(double i) {
		super(null, null);
		this.value = i;
		this.diff = () -> new ValueExpression(0);
		this.eval = () -> value;
		this.dupe = () -> this;
	}


	@Override
	String toInfix() {
		return ""+value;
	}		
	
}
