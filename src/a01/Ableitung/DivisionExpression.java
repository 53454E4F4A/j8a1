package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Division.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class DivisionExpression extends BaseExpression {

	DivisionExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.priority = 2;
		this.symbol = "/";
		this.diff = () ->  new DivisionExpression(
				// Z�hler
				new SubstractionExpression(
						new MultiplicationExpression(operand1.diff.get(), operand2.dupe.get()),
						new MultiplicationExpression(operand1.dupe.get(), operand2.diff.get())
				),
				// Nenner
						new MultiplicationExpression(operand2.dupe.get(), operand2.dupe.get())
				);
		
		this.eval = () -> operand1.eval.get() / operand2.eval.get();
		this.dupe = () -> new DivisionExpression(operand1.dupe.get(), operand2.dupe.get());
		this.optimize = () -> (this.operand2 instanceof ValueExpression) && this.operand2.eval.get() == 1. ? this.operand1 : this;
		
	}



	@Override
	String toInfix() {
		return "(" + this.operand1.toInfix() + " " + this.symbol + " " + this.operand2.toInfix() + ")";
	}



}
