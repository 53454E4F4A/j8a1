package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Quadratwurzel.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class SquarerootExpression extends BaseExpression {

	SquarerootExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.symbol = "sqrt";
		this.priority = 1;
		this.diff = () -> new MultiplicationExpression(
				new DivisionExpression(
						new ValueExpression(1),
						new MultiplicationExpression(new ValueExpression(1), new SquarerootExpression(operand1.dupe.get(), null))
						),
						operand1.diff.get()
						);
		this.dupe = () -> new SquarerootExpression(operand1.dupe.get(), null);
	}



	@Override
	String toInfix() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
