package a01.Ableitung;

import java.util.function.Function;
/**
 * Diese Klasse modeliert eine Addition
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class AdditionExpression extends BaseExpression {

	//Function<BaseExpression, BaseExpression> diff = f -> new AdditionExpression(f.operand1.diff(), f.operand2.diff());
	
	AdditionExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.symbol = "+";
		this.priority = 1;
		this.diff = () -> new AdditionExpression(this.operand1.diff.get(), this.operand2.diff.get());
		this.eval = () -> this.operand1.eval.get() + this.operand2.eval.get();
		this.dupe = () -> new AdditionExpression(this.operand1.dupe.get(), this.operand2.dupe.get());
	}


	@Override
	String toInfix() {
		return "(" + this.operand1.toInfix() + " " + this.symbol + " "+ this.operand2.toInfix() + ")";
	}
	

}
