package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Multiplikation.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class MultiplicationExpression extends BaseExpression {

	MultiplicationExpression(BaseExpression operand1, BaseExpression operand2) {
		
		super(operand1, operand2);
		this.symbol = "*";
		this.priority = 2;
		
		this.diff = () -> new AdditionExpression(
				new MultiplicationExpression(this.operand1.diff.get(), this.operand2.dupe.get()),
				new MultiplicationExpression(this.operand1.dupe.get(), this.operand2.diff.get())
				
				);
		
		this.eval = () -> this.operand1.eval.get() * this.operand2.eval.get();
		this.dupe = () -> new MultiplicationExpression(this.operand1.dupe.get(), this.operand2.dupe.get());
		// TODO Auto-generated constructor stub
	}



	@Override
	String toInfix() {
		return this.operand1.toInfix() + " " + this.symbol + " " + this.operand2.toInfix();
	}



}
