package a01.Ableitung;

import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		BaseExpression b = new PowerExpression(new VariableExpression(null, null), new ValueExpression(2));
		BaseExpression c = new MultiplicationExpression(new ValueExpression(4), b);
		System.out.println(c.diff.get().toInfix());
	}
	
}
