package a01.Ableitung;

import java.util.function.Function;
import java.util.function.Supplier;


/**
 * Abstrakte Basisklasse f�r Ausdr�cke.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */

public abstract class BaseExpression {
	 /**
     * erster Operand dieses Ausdrucks
     */
	BaseExpression operand1;
	/**
     * zweiter Operand dieses Ausdrucks
     */
	BaseExpression operand2;
	/**
     * Symbol des Operators
     */
	String symbol;
	/**
     * Priorit�t des Operators
     */
	int priority;
	/**
     * links/rechts Assoziation des Operators
     */
	boolean leftAssoz;
	
	/**
     * Gibt die Ableitung dieses Ausdrucks zur�ck
     */
	Supplier<BaseExpression> diff;
	/**
     * Gibt den Wert dieses Ausdrucks zur�ck
     */
	Supplier<Double> eval;
	/**
     * Dupliziert diesen Ausdruck
     */
	Supplier<BaseExpression> dupe;
	/**
     * Optimiert diesen Ausdruck
     */
	Supplier<BaseExpression> optimize;
	
	BaseExpression(BaseExpression operand1, BaseExpression operand2) {
		this.operand1 = operand1;
		this.operand2 = operand2;
		
	}
	/**
     * gibt den Ausdruck in Infix-Notation zur�ck
     */
	abstract String toInfix();

}
