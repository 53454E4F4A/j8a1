package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Potenz.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class PowerExpression extends BaseExpression {

	PowerExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.symbol = "^";
		this.priority = 3;
		this.leftAssoz = false;
		this.diff = () ->  new MultiplicationExpression(
				new PowerExpression(operand1.dupe.get(), operand2.dupe.get()),
				new AdditionExpression(
						new MultiplicationExpression(
								operand2.diff.get(), 
								new NaturalLogExpression(this.operand1.dupe.get(), null)),
								
						new DivisionExpression(
								new MultiplicationExpression(this.operand2.dupe.get(), this.operand1.diff.get()),
								operand1.dupe.get())
						)
				);
		this.eval = () -> Math.pow(operand1.eval.get(),  operand2.eval.get());
		this.dupe = () -> new PowerExpression(this.operand1.dupe.get(), this.operand2.dupe.get());
	}

	@Override
	String toInfix() {
		return "(" + this.operand1.toInfix() + this.symbol + this.operand2.toInfix() + ")";
	}



}
