package a01.Ableitung;
/**
 * Diese Klasse modeliert eine Substraktion.
 *
 * @author Jonas Sch�ufler
 * @version 1.0
 */
public class SubstractionExpression extends BaseExpression {

	SubstractionExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.priority = 1;
		this.symbol = "-";
		this.diff = () -> new SubstractionExpression(this.operand1.diff.get(), this.operand2.diff.get());
		this.eval = () -> this.operand1.eval.get() - this.operand2.eval.get();
		this.dupe = () -> new SubstractionExpression(operand1.dupe.get(), operand2.dupe.get());
	}


	@Override
	String toInfix() {
		return "(" + this.operand1.toInfix() + " " + this.symbol + " "+ this.operand2.toInfix() + ")";
	}

	

}
