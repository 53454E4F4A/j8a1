package a01.Ableitung;
/**
 * Diese Klasse modeliert einen natürlichen Logarithmus.
 *
 * @author Jonas Schäufler
 * @version 1.0
 */
public class NaturalLogExpression extends BaseExpression{

	NaturalLogExpression(BaseExpression operand1, BaseExpression operand2) {
		super(operand1, operand2);
		this.priority = 1;
		this.symbol = "ln";
		this.diff = () -> new MultiplicationExpression(
				new DivisionExpression(
						new ValueExpression(1),
						operand1.dupe.get()
			
				),
				operand1.diff.get());
		this.dupe = () -> null;
		this.eval = () -> null;
	}

	

	

	@Override
	String toInfix() {
	return this.symbol + this.operand1.toInfix()  ;
	}

	
}
