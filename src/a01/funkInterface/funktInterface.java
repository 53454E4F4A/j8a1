package a01.funkInterface;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 *
 */
public class funktInterface {

	/**
	 * Functional interface with an abstract function.
	 */
	@FunctionalInterface
	public interface IMyPoint {
		String toString(int x, int y);
	}

	public static void main(String[] args) {

		// Anonymous toString
		class MyPoint implements IMyPoint {

			@Override
			public String toString(int x, int y) {
				return "[ x = " + x + " ] " + " [ y = " + y + " ]";
			}
		}

		IMyPoint p1 = new MyPoint();
		System.out.println(p1.toString(10, 12));

		// Lambda toString
		IMyPoint p2 = (x, y) -> ("{ x = " + y + " } " + " { y = " + x + " }");
		System.out.println(p2.toString(45, 34));

		// Anonymous Runnable
		Runnable r1 = new Runnable() {

			@Override
			public void run() {
				System.out.println("Runnable without Lambda");
			}
		};

		// Lambda Runnable
		Runnable r2 = () -> {
			System.out.println("Runnable with Lambda!");
			System.out.println("bla blub");
		};

		// Run em!
		r1.run();
		r2.run();

		// Anonymous Thread
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("Thread without Lambda");
			}
		}).start();

		// Lambda Thread
		new Thread(() -> System.out.println("Thread with Lambda!")).start();

	}
}