package a01.Integral;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * 
 * Eine Klasse welche das Integral einer gegebenen Funktion mit Hilfe des
 * Simpson Verfahren approximiert
 * 
 * @author Aljoscha Czepoks
 * @author Jonas Sch�ufler
 * @version 1.0
 */

public class Integral {

	double a, b;
	int steps;
	double res = 0;
	
	/**
	 * Constructor 
	 *
	 * @param a: untere Grenze
	 * @param b: obere Grenze
	 * @param steps: anzahl der Schritte
	 * @param y: Funktion
	 * @return      instance
	 * @see         Simpson
	 */
	public Integral(double a, double b, int steps, Function<Double, Double> y) {
		this.a = a;
		this.b = b;
		this.steps = steps;
		this.y = y;
	}


	Function<Double, Double> y;

	Supplier<Double> increment = () -> (b - a) / steps;
	
	Function<Integer, Double> x = stepNo -> a + increment.get() * stepNo;

	Function<Integer, Double> Simpson = (stepNo) -> (stepNo == 0 || stepNo == steps) ? y
			.apply(x.apply(stepNo)) : y.apply(x.apply(stepNo))
			* ((stepNo % 2 == 0) ? 2. : 4.);

	/**
	 * Wendet Simpson-Regel auf eingestellte Werte mit einer einzelnen Loop an. 
	 * @return      aproximiertes Integral
	 *
	 */
			public double applyArray() {
				Double resArray[] = new Double[this.steps+1];
				res = 0;
				for (int i = 0; i <= this.steps; i++)
					resArray[i] = Simpson.apply(i);
				 new ArrayList<Double>(Arrays.asList(resArray)).forEach(x -> res+=x);
				return (increment.get() / 3) * res;
			}
	public double applyLoop() {

		res = 0;
		for (int i = 0; i <= this.steps; i++)
			res += Simpson.apply(i);
		return (increment.get() / 3) * res;
	}
	/**
	 * Wendet Simpson-Regel auf eingestellte Werte mit hilfe eines Arrays und forEach an. 
	 * @return      aproximiertes Integral
	 *
	 */
	public double apply() {
		res = 0;

		List<Integer> steps = new ArrayList<Integer>();
		List<Double> values = new ArrayList<Double>();
		for (int i = 0; i <= this.steps; i++) {
			steps.add((int) i);
		}
		steps.forEach(x -> values.add(Simpson.apply(x)));
		values.forEach(y -> res += y);

		return (increment.get() / 3) * res;
	}
	/**
	 * Wendet Simpson-Regel auf eingestellte Werte mit hilfe eines Arrays und forEach an, und erlaubt es neue Parameter festzulegen. 
	 * @return      aproximiertes Integral
	 *
	 */
	public double apply(double a, double b, int steps) {
		this.a = a;
		this.b = b;
		return apply();
	}
	/**
	 * Gibt einstellungen dieses Objects aus
	 * @return void
	 *
	 */
	public void printSettings() {
		System.out.println("a: " + a + " b:" + b);
		System.out.println("Steps: " + steps + " Increment:" + increment.get());
		System.out.println("x values:");
		for (int i = 0; i <= steps; i++)
			System.out.print(x.apply(i) + "\t");
		System.out.println("\ny values:");
		for (int i = 0; i <= steps; i++)
			System.out.print(y.apply(x.apply(i)) + "\t");
	}

}
