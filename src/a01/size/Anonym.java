package a01.size;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 *
 */
public class Anonym {

	public static void main(String[] args) {
		class MyPoint implements IMyPoint {

			@Override
			public String toString(int x, int y) {
				return "[ x = " + x + " ] " + " [ y = " + y + " ]";
			}
		}

		IMyPoint p1 = new MyPoint();
		System.out.println(p1.toString(10, 12));
	}

}
