package a01.size;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 *
 */

public class Lambda {

	public static void main(String[] args) {

		// Lambda toString
		IMyPoint p2 = (x, y) -> ("{ x = " + y + " } " + " { y = " + x + " }");
		System.out.println(p2.toString(45, 34));

	}
}