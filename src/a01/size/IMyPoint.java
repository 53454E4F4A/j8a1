package a01.size;

/**
 * @author Steffen Bredemeier, Aljoscha Czepoks, Jonas Schäufler & Piotr
 *         Konieczny
 *
 */
@FunctionalInterface
public interface IMyPoint {
	String toString(int x, int y);
}